package com.example.abde.squarerepo.repolist

import com.example.abde.squarerepo.data.Repository
import com.example.abde.squarerepo.data.source.ReposRepository
import com.example.abde.squarerepo.util.schedulers.BaseSchedulerProvider
import com.example.abde.squarerepo.util.schedulers.ImmediateSchedulerProvider
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class RepositoryViewModelTest {

    @Mock
    private lateinit var reposRepository: ReposRepository
    private lateinit var schedulerProvider: BaseSchedulerProvider
    private lateinit var repositoryViewModel: RepositoryViewModel
    private lateinit var testObserver: TestObserver<RepositoryViewState>
    private lateinit var repos: List<Repository>

    @Before
    fun setupRepositoryViewModel() {
        MockitoAnnotations.initMocks(this)

        schedulerProvider = ImmediateSchedulerProvider()

        repositoryViewModel = RepositoryViewModel(
            RepositoryActionProcessorHolder(
                reposRepository,
                schedulerProvider
            )
        )

        repos = listOf(
            Repository("name1", "desc1"),
            Repository("name2", "desc2"),
            Repository("name3", "desc3")
        )

        testObserver = repositoryViewModel.states().test()
    }

    @Test
    fun loaReposFromRepositoryAndLoadIntoView() {
        // Given an initialized RepositoryViewModel with initialized repos
        `when`(reposRepository.getRepos()).thenReturn(Single.just(repos))
        // When loading of Repos is initiated
        repositoryViewModel.processIntents(Observable.just(RepositoryIntent.InitialIntent))

        // Then progress indicator state is emitted
        testObserver.assertValueAt(1) { repoViewState ->
            repoViewState.isLoading
        }
        // Then progress indicator state is canceled and all repos are emitted
        testObserver.assertValueAt(2) { repoViewState ->
            println("repo: $repoViewState")
            !repoViewState.isLoading
        }
    }

    @Test
    fun refreshReposFromRepositoryAndLoadIntoView() {
        // Given an initialized RepositoryViewModel with initialized repos
        `when`(reposRepository.getRepos()).thenReturn(Single.just(repos))
        // When loading of Repos is initiated
        repositoryViewModel.processIntents(Observable.just(RepositoryIntent.RefreshIntent))

        // Then progress indicator state is emitted
        testObserver.assertValueAt(1) { repoViewState ->
            repoViewState.isLoading
        }
        // Then progress indicator state is canceled and all repos are emitted
        testObserver.assertValueAt(2) { repoViewState ->
            println("repo: $repoViewState")
            !repoViewState.isLoading
        }
    }

    @Test
    fun refreshReposFromRepositoryAndErrorResult() {
        // Given an initialized RepositoryViewModel with initialized repos
        `when`(reposRepository.getRepos()).thenReturn(Single.error(Throwable()))
        // When loading of Repos is initiated
        repositoryViewModel.processIntents(Observable.just(RepositoryIntent.RefreshIntent))

        // Then progress indicator state is emitted
        testObserver.assertValueAt(1) { repoViewState ->
            repoViewState.isLoading
        }
        // Then progress indicator state is canceled and error is emitted
        testObserver.assertValueAt(2) { repoViewState ->
            println("repo: $repoViewState")
            !repoViewState.isLoading && repoViewState.error != null
        }
    }
}