package com.example.abde.squarerepo.data.source

import com.example.abde.squarerepo.data.Repository
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class ReposRepositoryTest {
    private lateinit var repos: List<Repository>
    private lateinit var reposRepository: ReposRepository
    private lateinit var reposTestObserver: TestObserver<List<Repository>>
    @Mock
    private lateinit var reposRemoteDataSource: ReposDataSource

    @Before
    fun setupReposRepository() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this)

        // Get a reference to the class under test
        reposRepository = ReposRepository(reposRemoteDataSource)

        repos = listOf(
            Repository("name1", "desc1"),
            Repository("name2", "desc2"),
            Repository("name3", "desc3")
        )

        reposTestObserver = TestObserver()
    }

    @Test
    fun getRepos() {
        Mockito.`when`(reposRepository.getRepos()).thenReturn(Single.just(repos))

        val testObserver = TestObserver<List<Repository>>()
        reposRepository.getRepos().subscribe(testObserver)

        Mockito.verify<ReposDataSource>(reposRemoteDataSource).getRepos()
        testObserver.assertValue(repos)
    }

    @Test
    fun getRepos_withError() {
        val error = Throwable()
        Mockito.`when`(reposRepository.getRepos()).thenReturn(Single.error(error))

        val testObserver = TestObserver<List<Repository>>()
        reposRepository.getRepos().subscribe(testObserver)

        Mockito.verify<ReposDataSource>(reposRemoteDataSource).getRepos()
        testObserver.assertError(error)
    }
}