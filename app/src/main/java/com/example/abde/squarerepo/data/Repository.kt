package com.example.abde.squarerepo.data

data class Repository(val name: String?, val description: String?)