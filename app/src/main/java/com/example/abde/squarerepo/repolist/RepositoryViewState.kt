package com.example.abde.squarerepo.repolist

import com.example.abde.squarerepo.data.Repository
import com.example.abde.squarerepo.mvibase.MviViewState

data class RepositoryViewState(
    val isLoading: Boolean,
    val data: List<Repository>,
    val error: Throwable?
) : MviViewState {
    companion object {
        fun idle(): RepositoryViewState {
            return RepositoryViewState(
                isLoading = false,
                data = emptyList(),
                error = null
            )
        }
    }
}