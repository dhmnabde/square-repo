package com.example.abde.squarerepo.data.source.remote

import com.example.abde.squarerepo.data.Repository
import com.example.abde.squarerepo.data.source.ReposDataSource
import com.example.abde.squarerepo.data.source.ServiceGenerator
import com.example.abde.squarerepo.data.source.remote.base.Result
import com.example.abde.squarerepo.util.exhaustive
import io.reactivex.Single

class ReposRemoteDataSource(private val serviceGenerator: ServiceGenerator) : ReposDataSource {

    override fun getRepos(): Single<List<Repository>> {
        return Single.create { singleEmitter ->
            serviceGenerator.createService().repositories().run { result ->
                when (result) {
                    is Result.Success -> singleEmitter.onSuccess(result.data)
                    is Result.Error -> singleEmitter.onError(result.throwable)
                }.exhaustive
            }
        }
    }

}