package com.example.abde.squarerepo.util

import com.example.abde.squarerepo.data.source.ReposRepository
import com.example.abde.squarerepo.data.source.ServiceGenerator
import com.example.abde.squarerepo.data.source.remote.ReposRemoteDataSource
import com.example.abde.squarerepo.repolist.RepositoryActionProcessorHolder
import com.example.abde.squarerepo.repolist.RepositoryViewModel
import com.example.abde.squarerepo.util.schedulers.BaseSchedulerProvider
import com.example.abde.squarerepo.util.schedulers.SchedulerProvider
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

val appModule = module {

    // single instance of ServiceGenerator
    single { ServiceGenerator(this.androidContext()) }

    // single instance of ReposRemoteDataSource
    single(named("remote")) { ReposRemoteDataSource(get()) }

    // single instance of ReposRepository
    single { ReposRepository(get(named("remote"))) }

    // single instance of ReposRepository
    single<BaseSchedulerProvider> { SchedulerProvider }

    // single instance of RepositoryActionProcessorHolder
    single { RepositoryActionProcessorHolder(get(), get()) }

    viewModel { RepositoryViewModel(get()) }
}