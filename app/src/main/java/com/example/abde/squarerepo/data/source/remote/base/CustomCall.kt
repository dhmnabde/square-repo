package com.example.abde.squarerepo.data.source.remote.base

import android.os.Handler
import android.os.Looper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class CustomCall<R>(private val call: Call<R>) {

    fun run(responseHandler: (Result<R>) -> Unit) {
        try {
            val response = call.execute()
            handleResponse(response, responseHandler)
        } catch (t: IOException) {
            responseHandler(Result.Error(t))
        }
    }

    fun process(responseHandler: (Result<R>) -> Unit) {
        val callback = object : Callback<R> {
            override fun onFailure(call: Call<R>, t: Throwable) {
                callOnUIThread(responseHandler,
                    Result.Error(t)
                )
            }

            override fun onResponse(call: Call<R>, response: Response<R>) {
                handleResponse(response, responseHandler)
            }
        }

        call.enqueue(callback)
    }

    private fun handleResponse(response: Response<R>, handler: (Result<R>) -> Unit) {
        if (response.isSuccessful) {
            callOnUIThread(handler,
                Result.Success(
                    response.body()!!,
                    response.code()
                )
            )
        } else {
            callOnUIThread(handler,
                Result.Error(
                    Throwable("Bad api response"),
                    response
                )
            )
        }
    }

    private fun callOnUIThread(handler: (Result<R>) -> Unit, result: Result<R>) {
        val uiHandler = Handler(Looper.getMainLooper())
        uiHandler.post {
            handler(result)
        }
    }
}
