package com.example.abde.squarerepo.data.source.remote.base

import retrofit2.CallAdapter
import retrofit2.Retrofit
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

class CustomCallAdapterFactory private constructor() : CallAdapter.Factory() {

    override fun get(returnType: Type, annotations: Array<Annotation>, retrofit: Retrofit): CallAdapter<*, *>? {
        returnType.let {
            return try {
                val enclosingType = (it as ParameterizedType)

                if (enclosingType.rawType != CustomCall::class.java)
                    null
                else {
                    val type = enclosingType.actualTypeArguments[0]
                    CustomCallAdapter<Any>(type)
                }
            } catch (ex: ClassCastException) {
                null
            }
        }
    }

    companion object {
        @JvmStatic
        fun create() =
            CustomCallAdapterFactory()
    }
}
