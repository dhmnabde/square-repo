package com.example.abde.squarerepo.repolist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.abde.squarerepo.R
import com.example.abde.squarerepo.data.Repository
import com.example.abde.squarerepo.mvibase.BaseFragment
import com.example.abde.squarerepo.mvibase.MviView
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_repository.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class RepositoryFragment : BaseFragment(), MviView<RepositoryIntent, RepositoryViewState> {

    private val disposables = CompositeDisposable()
    private val repositoryViewModel: RepositoryViewModel by viewModel()
    private var repositoryAdapter: RepositoryAdapter? = null

    companion object {
        fun createInstance(): RepositoryFragment {
            return RepositoryFragment()
        }
    }

    //region Lifecycle

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_repository, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    override fun onDestroy() {
        super.onDestroy()

        disposables.dispose()
    }
    //endregion

    private fun init() {
        bind()

        repositoryAdapter = RepositoryAdapter()
        recyclerview_repository.apply {
            adapter = repositoryAdapter
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
        }
    }

    //region MviView implementation
    override fun intents(): Observable<RepositoryIntent> {
        return Observable.merge(
            initialIntent(),
            refreshIntent()
        )
    }

    override fun render(state: RepositoryViewState) {
        Timber.d("state: $state")
        button_repository_retry.visibility = View.GONE
        textview_repository_empty.visibility = View.GONE
        showLoading(state.isLoading)

        if (state.error != null) {
            showLoadingRepositoryError(state.error)
            return
        }

        if (state.data.isEmpty() && !state.isLoading) {
            showEmptyState(true)
        } else {
            showEmptyState(false)
            showList(state.data)
        }
    }
    //endregion

    private fun bind() {
        // Subscribe to the ViewModel and call render for every emitted state
        disposables.add(repositoryViewModel.states().subscribe(this::render))
        // Pass the UI's intents to the ViewModel
        repositoryViewModel.processIntents(intents())

    }

    //region Intent
    private fun initialIntent(): Observable<RepositoryIntent.InitialIntent> {
        return Observable.just(RepositoryIntent.InitialIntent)
    }

    private fun refreshIntent(): Observable<RepositoryIntent.RefreshIntent> {
        return button_repository_retry.clicks().map { RepositoryIntent.RefreshIntent }
    }
    //endregion

    private fun showLoading(isLoading: Boolean) {
        if (isAdded) {
            progressbar_repository.visibility = if (isLoading) View.VISIBLE else View.GONE
        }
    }

    private fun showLoadingRepositoryError(error: Throwable) {
        button_repository_retry.visibility = View.VISIBLE
        Timber.e("error $error")
    }

    private fun showList(data: List<Repository>) {
        repositoryAdapter?.data = data
    }

    private fun showEmptyState(isEmpty: Boolean) {
        if (isAdded) {
            textview_repository_empty.visibility = if (isEmpty) View.VISIBLE else View.GONE
        }
    }
}