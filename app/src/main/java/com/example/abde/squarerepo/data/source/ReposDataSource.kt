package com.example.abde.squarerepo.data.source

import com.example.abde.squarerepo.data.Repository
import io.reactivex.Single

interface ReposDataSource {
    fun getRepos(): Single<List<Repository>>
}