package com.example.abde.squarerepo.repolist

import com.example.abde.squarerepo.mvibase.MviIntent

sealed class RepositoryIntent: MviIntent {
    object InitialIntent : RepositoryIntent()

    object RefreshIntent : RepositoryIntent()


}