package com.example.abde.squarerepo.mvibase

import io.reactivex.Observable

interface MviView<I : MviIntent, in S : MviViewState> {
    /**
     *  Observable used by the MviViewModel to listen to the MviView.
     */
    fun intents(): Observable<I>

    /**
     * render MviView based on a MviViewState.
     */
    fun render(state: S)
}