package com.example.abde.squarerepo.mvibase

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

abstract class BaseActivity : AppCompatActivity() {

    fun addFragment(fragment: Fragment, layoutId: Int) {
        supportFragmentManager.beginTransaction().run {
            add(layoutId, fragment)
            commit()
        }
    }
}