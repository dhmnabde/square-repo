package com.example.abde.squarerepo.repolist

import android.os.Bundle
import android.os.PersistableBundle
import com.example.abde.squarerepo.R
import com.example.abde.squarerepo.mvibase.BaseActivity

class RepositoryActivity : BaseActivity() {

    //region Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repository)

        init()
    }
    //region

    private fun init() {
        addFragment(RepositoryFragment.createInstance(), R.id.framelayout_content)
    }

}