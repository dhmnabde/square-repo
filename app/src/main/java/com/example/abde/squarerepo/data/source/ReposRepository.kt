package com.example.abde.squarerepo.data.source

import com.example.abde.squarerepo.data.Repository
import io.reactivex.Single

open class ReposRepository(private val reposRemoteDataSource: ReposDataSource) : ReposDataSource {

    override fun getRepos(): Single<List<Repository>> {
        return reposRemoteDataSource.getRepos()
    }

}