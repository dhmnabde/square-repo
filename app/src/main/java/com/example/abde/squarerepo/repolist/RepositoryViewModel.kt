package com.example.abde.squarerepo.repolist

import androidx.lifecycle.ViewModel
import com.example.abde.squarerepo.mvibase.MviViewModel
import com.example.abde.squarerepo.util.exhaustive
import com.example.abde.squarerepo.util.notOfType
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.PublishSubject

class RepositoryViewModel(
    private val actionProcessorHolder: RepositoryActionProcessorHolder
) : ViewModel(), MviViewModel<RepositoryIntent, RepositoryViewState> {


    private val intentsSubject: PublishSubject<RepositoryIntent> = PublishSubject.create()
    private val statesObservable: Observable<RepositoryViewState> = compose()
    private val disposables = CompositeDisposable()

    private val intentFilter: ObservableTransformer<RepositoryIntent, RepositoryIntent>
        get() = ObservableTransformer { intents ->
            intents.publish { shared ->
                Observable.merge(
                    shared.ofType(RepositoryIntent.InitialIntent::class.java).take(1),
                    shared.notOfType(RepositoryIntent.InitialIntent::class.java)
                )
            }
        }

    //region MviViewModel implementation
    override fun processIntents(intents: Observable<RepositoryIntent>) {
        disposables.add(intents.subscribe(intentsSubject::onNext))
    }

    override fun states(): Observable<RepositoryViewState> {
        return statesObservable
    }
    //endregion

    /**
     * Compose all components to create the stream logic
     */
    private fun compose(): Observable<RepositoryViewState> {
        return intentsSubject
            .compose(intentFilter)
            .map(this::actionFromIntent)
            .compose(actionProcessorHolder.actionProcessor)
            .scan(RepositoryViewState.idle(), reducer)
            .distinctUntilChanged()
            .replay(1)
            .autoConnect(0)
    }

    private fun actionFromIntent(intent: RepositoryIntent): RepositoryAction {
        return when (intent) {
            RepositoryIntent.InitialIntent -> RepositoryAction.LoadRepoAction
            RepositoryIntent.RefreshIntent -> RepositoryAction.LoadRepoAction
        }.exhaustive
    }

    companion object {
        private val reducer =
            BiFunction { previousState: RepositoryViewState, result: RepositoryResult ->
                when (result) {
                    is RepositoryResult.LoadRepoResult -> when (result) {
                        is RepositoryResult.LoadRepoResult.Success -> {
                            previousState.copy(
                                isLoading = false,
                                data = result.repos,
                                error = null
                            )
                        }
                        is RepositoryResult.LoadRepoResult.Failure -> previousState.copy(
                            isLoading = false,
                            error = result.error
                        )
                        is RepositoryResult.LoadRepoResult.InFlight -> previousState.copy(
                            isLoading = true,
                            error = null
                        )
                    }.exhaustive
                }.exhaustive
            }

    }

}