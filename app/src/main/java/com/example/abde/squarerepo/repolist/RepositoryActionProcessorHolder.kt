package com.example.abde.squarerepo.repolist

import com.example.abde.squarerepo.data.source.ReposRepository
import com.example.abde.squarerepo.util.schedulers.BaseSchedulerProvider
import io.reactivex.Observable
import io.reactivex.ObservableTransformer

class RepositoryActionProcessorHolder(
    private val reposRepository: ReposRepository,
    private val schedulerProvider: BaseSchedulerProvider
) {

    private val loadReposProcessor =
        ObservableTransformer<RepositoryAction.LoadRepoAction, RepositoryResult.LoadRepoResult> { actions ->
            actions.flatMap { action ->
                reposRepository.getRepos()
                    .toObservable()
                    .map {
                        RepositoryResult.LoadRepoResult.Success(it)
                    }
                    .cast(RepositoryResult.LoadRepoResult::class.java)
                    .onErrorReturn(RepositoryResult.LoadRepoResult::Failure)
                    .subscribeOn(schedulerProvider.io())
                    .observeOn(schedulerProvider.ui())
                    .startWith(RepositoryResult.LoadRepoResult.InFlight)
            }
        }

    internal var actionProcessor =
        ObservableTransformer<RepositoryAction, RepositoryResult> { actions ->
            actions.publish { shared ->
                Observable.merge(
                    shared.ofType(RepositoryAction.LoadRepoAction::class.java).compose(
                        loadReposProcessor
                    ),
                    shared.filter { action ->
                        action !is RepositoryAction.LoadRepoAction
                    }.flatMap { action ->
                        Observable.error<RepositoryResult>(IllegalArgumentException("Unknown Action type: $action"))
                    })
                //)
            }
        }
}