package com.example.abde.squarerepo.util

import io.reactivex.Observable

fun <T : Any, U : Any> Observable<T>.notOfType(clazz: Class<U>): Observable<T> {
    return filter { !clazz.isInstance(it) }
}