package com.example.abde.squarerepo.repolist

import com.example.abde.squarerepo.mvibase.MviAction

sealed class RepositoryAction : MviAction {
    object LoadRepoAction : RepositoryAction()
}