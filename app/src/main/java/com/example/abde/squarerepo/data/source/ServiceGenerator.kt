package com.example.abde.squarerepo.data.source

import android.content.Context
import com.example.abde.squarerepo.BuildConfig
import com.example.abde.squarerepo.R
import com.example.abde.squarerepo.data.source.remote.base.AppApi
import com.example.abde.squarerepo.data.source.remote.base.CustomCallAdapterFactory
import com.github.simonpercic.oklog3.OkLogInterceptor
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.moczul.ok2curl.CurlInterceptor
import com.moczul.ok2curl.Options
import com.moczul.ok2curl.logger.Loggable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit

class ServiceGenerator(private val context: Context) {

    private val okLogInterceptor: OkLogInterceptor
    private val httpLoggingInterceptor: HttpLoggingInterceptor
    private val curlInterceptor: CurlInterceptor
    private val gson: Gson

    init {
        okLogInterceptor = createOkLogInterceptor()

        gson = createGson()

        var outBuffer = ""
        var inBuffer = ""
        httpLoggingInterceptor = HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
            override fun log(message: String) {
                when {
                    message.startsWith("--> END") -> {
                        outBuffer += "\n" + message
                        Timber.tag("OkHttp").d(outBuffer)
                        outBuffer = ""
                    }
                    message.startsWith("<-- END") -> {
                        inBuffer += "\n" + message
                        Timber.tag("OkHttp").d(inBuffer)
                        inBuffer = ""
                    }
                    message.startsWith("--> ") -> outBuffer += message
                    message.startsWith("<-- ") -> inBuffer += message
                    outBuffer.isNotEmpty() -> outBuffer += "\n" + message
                    inBuffer.isNotEmpty() -> inBuffer += "\n" + message
                }            }
        })
        httpLoggingInterceptor.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE

        curlInterceptor = createCurlInterceptor()
    }

    private fun createOkLogInterceptor(): OkLogInterceptor {
        return OkLogInterceptor.builder()
            .withAllLogData()
            .build()
    }

    private fun createCurlInterceptor(): CurlInterceptor {
        val options = Options.builder()
            .compressed()
            .build()
        return CurlInterceptor(Loggable {
            Timber.tag("Ok2Curl").d(it)
        }, options)
    }

    private fun createGson(): Gson {
        return GsonBuilder()
            .create()
    }

    fun createService(): AppApi {
        val endpointUrl = context.getString(R.string.api_endpoint)

        val okHttpClientBuilder = OkHttpClient().newBuilder()
            .connectTimeout(1, TimeUnit.MINUTES)
            .readTimeout(1, TimeUnit.MINUTES)
            .writeTimeout(1, TimeUnit.MINUTES)

        if (BuildConfig.DEBUG) {
            okHttpClientBuilder.addInterceptor(httpLoggingInterceptor)
                .addInterceptor(okLogInterceptor)
                .addNetworkInterceptor(curlInterceptor)
        }

        val retrofit = Retrofit.Builder()
            .baseUrl(endpointUrl)
            .client(okHttpClientBuilder.build())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(CustomCallAdapterFactory.create())
            .build()

        return retrofit.create(AppApi::class.java)
    }
}
