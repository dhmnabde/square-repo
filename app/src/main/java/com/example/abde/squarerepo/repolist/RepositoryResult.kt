package com.example.abde.squarerepo.repolist

import com.example.abde.squarerepo.data.Repository
import com.example.abde.squarerepo.mvibase.MviResult

sealed class RepositoryResult : MviResult {
    sealed class LoadRepoResult : RepositoryResult() {
        data class Success(val repos: List<Repository>) : LoadRepoResult()
        data class Failure(val error: Throwable) : LoadRepoResult()
        object InFlight : LoadRepoResult()
    }
}