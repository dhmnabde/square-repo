package com.example.abde.squarerepo.data.source.remote.base

import com.example.abde.squarerepo.data.Repository
import retrofit2.http.GET

interface AppApi {
    @GET("orgs/square/repos")
    fun repositories(): CustomCall<List<Repository>>
}