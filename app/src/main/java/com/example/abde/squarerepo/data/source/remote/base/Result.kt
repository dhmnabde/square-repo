package com.example.abde.squarerepo.data.source.remote.base

import retrofit2.Response

sealed class Result<out T> {
    data class Success<out T>(val data: T, val code: Int) : Result<T>()
    data class Error<T>(val throwable: Throwable, val data: Response<T>? = null) : Result<T>()
}
