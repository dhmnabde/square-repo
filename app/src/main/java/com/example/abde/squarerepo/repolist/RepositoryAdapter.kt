package com.example.abde.squarerepo.repolist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.abde.squarerepo.R
import com.example.abde.squarerepo.data.Repository
import kotlinx.android.synthetic.main.cell_repository.view.*

class RepositoryAdapter : RecyclerView.Adapter<RepositoryAdapter.ViewHolder>() {

    var data: List<Repository>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(repository: Repository?) = with(itemView) {
            repository?.let {
                textview_cell_repository_name.text = it.name
                textview_cell_repository_description.text = it.description
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cell_repository, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data?.get(position))
    }

    override fun getItemCount(): Int {
        return data?.size ?: 0
    }
}