# Square Repositories
Display scrollable list of repositories of “square” organisation in GitHub.

Architecture used: MVI

Main libraries used :
- Retrofit
- RxJava
- Koin
